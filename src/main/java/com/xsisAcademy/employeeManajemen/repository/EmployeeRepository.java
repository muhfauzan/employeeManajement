package com.xsisAcademy.employeeManajemen.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsisAcademy.employeeManajemen.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	List<Employee> findByIsActive(Boolean isActive);
}