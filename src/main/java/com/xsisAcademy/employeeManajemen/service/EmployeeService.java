package com.xsisAcademy.employeeManajemen.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsisAcademy.employeeManajemen.model.Employee;
import com.xsisAcademy.employeeManajemen.repository.EmployeeRepository;

@Service
public class EmployeeService {
	@Autowired
	private EmployeeRepository employeeRepository;

	public EmployeeService(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;
	}

	public List<Employee> getAllEmployees() {
		return employeeRepository.findByIsActive(true);
	}

	public Optional<Employee> findEmployeeById(Long id) {
		return employeeRepository.findById(id);
	}

	public Employee saveEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}

	public void deleteEmployee(Long id) {
		Optional<Employee> employee = employeeRepository.findById(id);
		employee.get().setIsActive(false);
	}

	public Double getAverageSalary() {
		return employeeRepository.findByIsActive(true).stream().mapToDouble(Employee::getSalary).average().orElse(0.0);
	}
}