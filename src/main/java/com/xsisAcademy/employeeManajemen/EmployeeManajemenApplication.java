package com.xsisAcademy.employeeManajemen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeManajemenApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeManajemenApplication.class, args);
	}

}
