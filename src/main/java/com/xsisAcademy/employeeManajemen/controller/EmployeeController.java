package com.xsisAcademy.employeeManajemen.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.xsisAcademy.employeeManajemen.model.Employee;
import com.xsisAcademy.employeeManajemen.service.EmployeeService;

@RestController
public class EmployeeController {
  private final EmployeeService employeeService;

  public EmployeeController(EmployeeService employeeService) {
    this.employeeService = employeeService;
  }

  @GetMapping("/employees")
  public List<Employee> getAllEmployees() {
    return employeeService.getAllEmployees();
  }

  @GetMapping("/employees/{id}")
  public ResponseEntity<Employee> findEmployeeById(@PathVariable Long id) {
    Optional<Employee> employee = employeeService.findEmployeeById(id);
    return employee.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
  }

  @PostMapping("/employees/add")
  public Employee saveEmployee(@RequestBody Employee employee) {
    return employeeService.saveEmployee(employee);
  }

  @PostMapping("/employees/{id}")
  public void deleteEmployee(@PathVariable Long id) {
    employeeService.deleteEmployee(id);
  }

  @GetMapping("/employees/average-salary")
  public Double getAverageSalary() {
    return employeeService.getAverageSalary();
  }
}